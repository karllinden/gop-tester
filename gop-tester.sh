#!/bin/bash
#
#  Copyright 2015, 2017 Karl Linden <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

export CFLAGS="-Werror"

# features to brute force check (in all packages)
probe=(
    doc
    examples
    nls
    static-libs
)

bold="\e[1m"
normal="\e[0m"

default="\e[39m"
red="\e[91m"

workdir=`pwd`
builddir="${workdir}/build"
installdir="${workdir}/install"

projects=(
    gop
    cotesdex
    gop-cotesdex
)

# Parse arguments. Writing a good shell script argument parser is waste
# of time.
custom_dirs=()
probe_projects=()
while [ $# -ge 1 ]
do
    found_flag=false
    for project in ${projects[@]}
    do
        if [ "${1}" = "--${project}-dir" ]
        then
            found_flag=true
            shift
            custom_dirs+=("${project}|${1}")
            shift
        fi
    done
    if ! ${found_flag}
    then
        probe_projects+=("$1")
        shift
    fi
done

is_default_dir() {
    if [ $# -ne 1 ]
    then
        die "${0}: invalid number of arguments"
    fi

    local project="${1}"
    for x in "${custom_dirs[@]}"
    do
        this_project=`echo ${x} | cut -d "|" -f 1`
        if [ "${this_project}" = "${project}" ]
        then
            return 1
        fi
    done

    return 0
}

get_dir() {
    if [ $# -ne 1 ]
    then
        die "${0}: invalid number of arguments"
    fi

    local project=${1}
    for x in "${custom_dirs[@]}"
    do
        local this_project=`echo ${x} | cut -d "|" -f 1`
        local this_dir=`echo ${x} | cut -d "|" -f 2`
        if [ "${this_project}" = "${project}" ]
        then
            echo "${this_dir}"
            return
        fi
    done

    echo "${builddir}"/"${project}"
}

get_upstream() {
    if [ $# -ne 1 ]
    then
        die "${0}: invalid number of arguments"
    fi

    local project=${1}
    echo https://gitlab.com/karllinden/${project}.git
}
atexit() {
    for project in ${projects[@]}
    do
        try find "$(get_dir ${project})" -type f -execdir chmod u+w {} +
    done
}

msg() {
    echo -e "(`realpath --relative-to=.. .`) ${bold}${@}${normal}"
}

die() {
    echo -e "${red}${bold}${@}${normal}${default}"
    atexit
    exit 1
}

run() {
    msg "${@}"
    "${@}" || die "${@} failed"
}

try() {
    msg "${@}"
    "${@}" || echo "${@} failed"
}

qrun() {
    msg "${@}"
    "${@}" > /dev/null || die "${@} failed"
}

# USAGE: build_real project [args...] -- [next packages...]
build_real() {
    local project=${1}
    local dir="$(get_dir ${project})"
    shift

    local args=()
    while [ "${1}" != "--" ]
    do
        args+=("${1}")
        shift
    done
    shift

    # Enter project directory.
    qrun pushd "${dir}"

    # Save waf_help in a file so that some options can be queried.
    local waf_help=$(mktemp)
    run ./waf --help > "${waf_help}"

    # If there is a --tests option to the ./waf configure make sure to
    # add it so that configure fails if the programs needed for running
    # tests do not exist.
    local extra_args=()
    if grep "\-\-tests[[:space:]]" "${waf_help}" > /dev/null
    then
        extra_args+=(--tests)
    fi

    run ./waf configure \
        --prefix="${installdir}"/${project} \
        "${args[@]}" "${extra_args[@]}"
    run ./waf -v

    # Call ./waf check if it is implemented.
    if grep "[[:space:]]\{1,\}check[[:space:]]*:" "${waf_help}" \
        > /dev/null
    then
        run ./waf check -v
    fi
    rm "${waf_help}"

    run ./waf install

    if [ $# -gt 0 ]
    then
        build "${@}"
    fi

    # Uninstall the package and make sure there are no leftover files.
    # In fact, waf removes empty directories, so a check to see if the
    # directory is left is sufficient.
    run ./waf uninstall
    if [ -e "${installdir}"/${project} ]
    then
        die "install directory exists after uninstall"
    fi

    # Make sure the next build of this project is clean.
    run rm -rf build

    # Leave the project directory.
    qrun popd
}

# USAGE: build_recursive project [features...] -- [args...] -- [next packages]
build_recursive() {
    local project=${1}
    shift

    local feature=${1}
    shift

    if [ "${feature}" == "--" ]
    then
        build_real ${project} "${@}"
        return
    fi

    local features=()
    while [ "${1}" != "--" ]
    do
        features+=("${1}")
        shift
    done
    shift

    local args=()
    while [ "${1}" != "--" ]
    do
        args+=("${1}")
        shift
    done
    shift

    build_recursive ${project} ${features[@]} -- "${args[@]}" --${feature} -- "${@}"
    build_recursive ${project} ${features[@]} -- "${args[@]}" --no-${feature} -- "${@}"
}

build() {
    local project=${1}
    local dir="$(get_dir ${project})"
    shift

    # Enter project directory.
    qrun pushd "${dir}"

    # Construct the array of features of this project, if this project
    # is in the projects to probe.
    local features=()
    for p in ${probe_projects[@]}
    do
        if [ ${p} = ${project} ]
        then
            local tmp=$(mktemp)
            run ./waf --help > "${tmp}"
            for feature in ${probe[@]}
            do
                if grep -F -- "--${feature}" < "${tmp}" > /dev/null
                then
                    features+=("${feature}")
                fi
            done
            rm "${tmp}"
            break
        fi
    done

    # Leave project directory.
    qrun popd

    build_recursive ${project} ${features[@]} -- -- "${@}"
}

main() {
    for project in ${projects[@]}
    do
        export PATH="${installdir}/${project}/bin:${PATH}"
        export LD_LIBRARY_PATH="${installdir}/${project}/lib:${LD_LIBRARY_PATH}"
        export PKG_CONFIG_PATH="${installdir}/${project}/lib/pkgconfig:${PKG_CONFIG_PATH}"
    done

    export COTESDEX=cotesdex-valgrind
    export COTESDEXFLAGS=--memcheck

    # Remove the install directory, because it is tested that waf
    # uninstall completely. Such a test fails if there are leftover
    # files since a previous test.
    run rm -rf "${installdir}"

    # Create the build directory.
    mkdir -p "${builddir}"

    # Create or update the projects.
    for project in ${projects[@]}
    do
        local dir=$(get_dir ${project})

        # Only fetch from the local repository if the project is in its
        # default directory.
        if is_default_dir ${project}
        then
            # Update the project directory if it exists. Otherwise pull
            # the project.
            if [ -d "${dir}" ]
            then
                qrun pushd "${dir}"
                run git clean -fdx
                run git checkout master
                run git pull
                qrun popd
            else
                run git clone $(get_upstream ${project}) "${dir}"
            fi
        fi

        qrun pushd "${dir}"
        run git submodule update --init
        run rm -rf build .lock-waf*
        run find -type f -execdir chmod a-w,g-rw,o-rw {} +
        qrun popd
    done

    build ${projects[@]}

    atexit
}

if [ "${0}" != "bash" -a "${0}" != "-bash" ]
then
    main ${@}
fi
